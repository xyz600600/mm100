#!/bin/python

import sys

def get_score(seed):
    with open("results/1/%d/stdout" % seed) as fin:
        return float(fin.readline().strip().split("=")[1].strip())

max_seed = int(sys.argv[1])

def mean(lst):
    return sum(lst) / len(lst)

print mean(map(get_score, range(1, max_seed + 1)))
