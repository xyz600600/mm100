#!/bin/bash

if [ ! -e results ]; then
	mkdir results
fi

num=2000

parallel --progress --results results java -jar tester.jar -exec "./a.out" -novis -seed $1 ::: `seq 1 $num`

python collect.py $num > results.txt

rm -r results


