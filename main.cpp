// C++11
#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <map>
#include <sstream>
#include <vector>
#include <set>
#include <string>
#include <cassert>
#include <sstream>

using namespace std;

#define DEBUG

const bool IS_DEBUG = false;

const int USED = -1;
const int NOT_USED = 0;
const int TESTED = -2;
const int DELETE_TESTED = -3;

const int WALL = -1;
const int LEFT = 0;
const int UP = 1;
const int RIGHT = 2;
const int DOWN = 3;

struct Pos
{
  int y;
  int x;

  Pos(int y, int x)
	: y(y)
	, x(x){}

  Pos(){}
};

ostream &operator<<(ostream &out, const Pos &p)
{
  out << "(" << p.y << ", " << p.x << ")";
  return out;
}

int encode(const Pos &p)
{
  return p.y * 128 + p.x;
}

bool operator<(const Pos &p1, const Pos &p2)
{
  return encode(p1) < encode(p2);
}

bool operator!=(const Pos &p1, const Pos &p2)
{
  return p1.y != p2.y || p1.x != p2.x;
}

struct Cell
{
  int color;
  array<Cell *, 4> next;
  int visited;
  Pos pos;
};

struct Board
{
  int H;
  int W;
  vector<vector<Cell>> board;

  Board(const vector<string> &board);
  bool is_alone(const int y, const int x) const;
  bool is_alone(const Pos &p) const;
  int calc_min_area(int y, int x);
  
  void remove(const Pos &p);
  void remove(const int y, const int x);
  void remove(Cell *c);

  void undo(const Pos &p);
  void undo(const int y, const int x);
  void undo(Cell *c);
  
  int color(const int y, const int x) const;
  int color(const Pos &p) const;

  int &visited(const int y, const int x);
  int &visited(const Pos &p);

  Cell *cell(const int y, const int x);
  Cell *cell(const Pos &p);
  
  Cell *next(const int y, const int x, const int dir);
  Cell *next(const Pos &p, const int dir);
};

Cell *Board::cell(const int y, const int x)
{
  return &board[y][x];
}

Cell *Board::cell(const Pos &p)
{
  return cell(p.y, p.x);
}

void Board::undo(const Pos &p)
{
  undo(p.y, p.x);
}

void Board::undo(const int y, const int x)
{
  undo(&board[y][x]);
}

void Board::undo(Cell *center)
{
  center->next[LEFT]->next[RIGHT] = center;
  center->next[RIGHT]->next[LEFT] = center;
  center->next[UP]->next[DOWN] = center;
  center->next[DOWN]->next[UP] = center;
}

Cell *Board::next(const int y, const int x, const int dir)
{
  return board[y][x].next[dir];
}

Cell *Board::next(const Pos &p, const int dir)
{
  return next(p.y, p.x, dir);
}

int &Board::visited(const int y, const int x)
{
  return board[y][x].visited;
}

int &Board::visited(const Pos &p)
{
  return visited(p.y, p.x);
}

int Board::color(const int y, const int x) const
{
  return board[y][x].color;
}

int Board::color(const Pos &p) const
{
  return color(p.y, p.x);
}

void Board::remove(const Pos &p)
{
  remove(p.y, p.x);
}

void Board::remove(const int y, const int x)
{
  remove(&board[y][x]);
}

void Board::remove(Cell *center)
{
  center->next[LEFT]->next[RIGHT] = center->next[RIGHT];
  center->next[RIGHT]->next[LEFT] = center->next[LEFT];
  center->next[UP]->next[DOWN] = center->next[DOWN];
  center->next[DOWN]->next[UP] = center->next[UP];
}

int Board::calc_min_area(const int y, const int x)
{
  int min_area = 100;
  
  for (int i = max(1, y - 5); i <= min(H, y + 5); i++)
  {
	for (int j = max(1, x - 5); j <= min(W, x + 5); j++)
	{
	  if ((i != y || j != x) && board[y][x].color == board[i][j].color)
	  {
		const int area = (abs(y - i) + 1) * (abs(j - x) + 1);
		min_area = min(min_area, area);
	  }
	}
  }
  return min_area;
}

bool Board::is_alone(const Pos &p) const
{
  return is_alone(p.y, p.x);
}

bool Board::is_alone(const int y, const int x) const
{
  auto &c = board[y][x];
  return c.color != c.next[LEFT]->color &&
	c.color != c.next[RIGHT]->color &&
	c.color != c.next[UP]->color &&
	c.color != c.next[DOWN]->color;
}

Board::Board(const vector<string> &board_str)
{
  H = board_str.size();
  W = board_str[0].size();

  board.resize(H + 2);
  for (auto &v : board)
  {
	v.resize(W + 2);
	for (auto &vv : v)
	{
	  vv.color = WALL;
	}
  }

  for (int i = 1; i <= H; i++)
  {
	for (int j = 1; j <= W; j++)
	{
	  Cell &c = board[i][j];
	  c.color = board_str[i-1][j-1] - '0';

	  c.next[LEFT] = &board[i][j - 1];
	  c.next[RIGHT] = &board[i][j + 1];
	  c.next[UP] = &board[i - 1][j];
	  c.next[DOWN] = &board[i + 1][j];

	  c.visited = NOT_USED;
	  c.pos = Pos(i, j);
	}
  }
}

class SameColorPairs
{
public:
  vector<string> removePairs(const vector<string> &board_str);
  
private:

  static const int MAX_DEPTH = 5;
  
  void select_order(Board &board, vector<Pos> &pos_list, int iter);
  bool match(Board &board, const Pos &p, const int depth, vector<Cell *> &s);
  void initialize_neighbor_list(Board &board, const Pos &p, vector<Pos> &neighbor_list);
  void reorder_neighbor(Board &board, const Pos &p, vector<Pos> &neighbor_list);

  string to_string(const Pos &p1, const Pos &p2);
};

// Mark の相互参照は、ここで除外
void SameColorPairs::reorder_neighbor(Board &board, const Pos &p, vector<Pos> &neighbor_list)
{
  vector<pair<int, Pos>> vs;
  for (auto &np : neighbor_list)
  {
	int area = 0;
	int prohibited = 0;
	for (int y = min(p.y, np.y); y <= max(p.y, np.y); y++)
	{
	  for (int x = min(p.x, np.x); x <= max(p.x, np.x); x++)
	  {
		if (board.visited(y, x) == TESTED)
		{
		  prohibited = -10000;
		}
		area += board.is_alone(p);
	  }
	}
	const int priority = area - 2 + prohibited;
	if (priority >= 0)
	{
	  vs.push_back(make_pair(priority, np));
	}
  }
  sort(vs.begin(), vs.end());

  neighbor_list.clear();
  for(auto &v : vs)
  {
	neighbor_list.push_back(v.second);
  }
}

// 解消できたらtrue
bool SameColorPairs::match(Board &board, const Pos &p, const int depth, vector<Cell *> &s)
{
  if (depth == MAX_DEPTH)
  {
	return false;
  }
  else
  {
	if (IS_DEBUG) cerr << "start to match: " << p << " " << depth << endl;
	
	vector<Pos> neighbor_list;
	initialize_neighbor_list(board, p, neighbor_list);

	reorder_neighbor(board, p, neighbor_list);
	if (neighbor_list.size() >= 5) neighbor_list.resize(5);

	assert(board.visited(p) == NOT_USED);
	board.visited(p) = TESTED;

	const int base_offset = s.size();
	
	// 長方形の列挙
	for (const Pos &np : neighbor_list)
	{
	  assert(board.visited(np) == NOT_USED);
	  board.visited(np) = TESTED;

	  if (IS_DEBUG) cerr << "selected " << np << " to match " << p << endl; 
	  
	  vector<pair<bool, Pos>> pos_rem;
	  // この中を消さないといけない．消しやすい順
	  for (int y = min(p.y, np.y); y <= max(p.y, np.y); y++)
	  {
		for (int x = min(p.x, np.x); x <= max(p.x, np.x); x++)
		{
		  Pos p2(y, x);
		  if (board.visited(p2) == NOT_USED && p != p2 && np != p2)
		  {
			pos_rem.push_back(make_pair(board.is_alone(y, x), p2));
		  }
		}
	  }
	  sort(pos_rem.begin(), pos_rem.end());

	  bool success = true;
	  
	  for (auto &p_p2 : pos_rem)
	  {
		auto &p2 = p_p2.second;

		if (IS_DEBUG) cerr << "should remove " << p2 << endl;
		
		if (board.visited(p2) == TESTED || board.visited(p2) == DELETE_TESTED)
		{
		  if (IS_DEBUG) cerr << "this is already deleted. " << endl;
		  continue;
		}
		
		// 消せる場合は，greedyに消す
		bool deleted = false;
		for (int i = 0; i < 4; i++)
		{
		  if (board.color(p2) == board.next(p2, i)->color && board.next(p2, i)->visited == NOT_USED)
		  {
			if (IS_DEBUG) cerr << "match greedyly. @ " << board.next(p2, i)->pos << endl;
			
			s.push_back(board.cell(p2));
			board.visited(p2) = DELETE_TESTED;
			
			s.push_back(board.next(p2, i));
			board.next(p2, i)->visited = DELETE_TESTED;
			
			board.remove(p2);
			board.remove(board.next(p2, i));
			
			deleted = true;
			break;
		  }
		}
		if (!deleted)
		{
		  if (IS_DEBUG) cerr << "match recursively. @ " << endl;
		  if (!match(board, p2, depth + 1, s))
		  {
			success = false;
			break;
		  }
		}
	  }
	  if (success)
	  {
		if (IS_DEBUG) cerr << "success to match " << p << " " << np << endl;
		board.remove(p);
		s.push_back(board.cell(p));
		board.visited(p) = DELETE_TESTED;
		
		board.remove(np);
		s.push_back(board.cell(np));
		board.visited(np) = DELETE_TESTED;
		
		return true;
	  }
	  else
	  {
		if (IS_DEBUG) cerr << "fail to match " << p << " " << np << endl;
		while (s.size() > base_offset)
		{
		  Cell *c = s.back();
		  s.pop_back();
		  assert(c->visited == TESTED || c->visited == DELETE_TESTED);
		  board.undo(c->pos);
		  c->visited = NOT_USED;
		}
		board.visited(np) = NOT_USED;
	  }
	}
	board.visited(p) = NOT_USED;
  }
  return false;
}

void SameColorPairs::select_order(Board &board, vector<Pos> &pos_list, int iter)
{
  vector<pair<int, Pos>> order_with_priority;
  
  for (int i = 1; i <= board.H; i++)
  {
	for (int j = 1; j <= board.W; j++)
	{
	  if (board.cell(i, j)->visited == NOT_USED)
	  {
		int priority = 0;
		if (board.is_alone(i, j))
		{
		  priority -= 10000;
		}
		if (iter == 0)
		{
		  priority -= board.calc_min_area(i, j);
		}
		else
		{
		  vector<Pos> neighbor_list;
		  initialize_neighbor_list(board, board.cell(i, j)->pos, neighbor_list);

		  int min_area = 100000000;
		  for (auto &v : neighbor_list)
		  {
			const int area = (abs(v.y - i) + 1) * (abs(j - v.x) + 1);
			min_area = min(min_area, area);
		  }
		  priority -= min_area;
		}
		order_with_priority.push_back(make_pair(priority, Pos(i, j)));
	  }
	}
  }
  sort(order_with_priority.begin(), order_with_priority.end());

  for (auto &v : order_with_priority)
  {
	if (IS_DEBUG) cerr << v.first << " " << v.second << endl;
	pos_list.push_back(v.second);
  }  
}

// 菱形風に近傍を取る
void SameColorPairs::initialize_neighbor_list(Board &b, const Pos &p, vector<Pos> &neighbor_list)
{
  const int y = p.y;
  const int x = p.x;
  
  int min_dx = -x + 1;
  int min_dx_0;
  int dy = 0;
  bool finish = false;
  while (0 < y + dy && !finish)
  {
	int dx = (dy == 0 ? -1 : 0);
	while (min_dx <= dx)
	{
	  if (b.visited(y + dy, x + dx) == NOT_USED && b.color(y, x) == b.color(y + dy, x + dx))
	  {
		break;
	  }
	  dx--;
	}
	if (dy == 0)
	{
	  min_dx_0 = dx;
	}		  
	if (min_dx <= dx)
	{
	  min_dx = dx + 1;
	  neighbor_list.push_back(Pos(y + dy, x + dx));
	  finish = (min_dx > 0);
	}
	dy--;
  }

  min_dx = min_dx_0 + 1;
  dy = 1;
  finish = false;
  while (y + dy <= b.H && !finish)
  {
	int dx = 0;
	while (min_dx <= dx)
	{
	  if (b.visited(y + dy, x + dx) == NOT_USED && b.color(y, x) == b.color(y + dy, x + dx))
	  {
		break;
	  }
	  dx--;
	}
	if (min_dx <= dx)
	{
	  min_dx = dx + 1;
	  neighbor_list.push_back(Pos(y + dy, x + dx));
	  finish = (min_dx > 0);
	}
	dy++;
  }

  int max_dx = b.W - x;
  int max_dx_0;
  dy = 0;
  finish = false;
  while (0 < y + dy && !finish)
  {
	int dx = 1;
	while (dx <= max_dx)
	{
	  if (b.visited(y + dy, x + dx) == NOT_USED && b.color(y, x) == b.color(y + dy, x + dx))
	  {
		break;
	  }
	  dx++;
	}
	if (dy == 0)
	{
	  max_dx_0 = dx;
	}
	if (dx <= max_dx)
	{
	  max_dx = dx - 1;
	  neighbor_list.push_back(Pos(y + dy, x + dx));
	  finish = (max_dx == 0);
	}
	dy--;
  }

  max_dx = max_dx_0 - 1;
  dy = 1;
  finish = false;
  while (y + dy < b.H && !finish)
  {
	int dx = 1;
	while (dx <= max_dx)
	{
	  if (b.visited(y + dy, x + dx) == NOT_USED && b.color(y, x) == b.color(y + dy, x + dx))
	  {
		break;
	  }
	  dx++;
	}
	if (dx <= max_dx)
	{
	  max_dx = dx - 1;
	  neighbor_list.push_back(Pos(y + dy, x + dx));
	  finish = (max_dx == 0);
	}
	dy++;
  }
}

string SameColorPairs::to_string(const Pos &p1, const Pos &p2)
{
  stringstream ss;
  ss << (p1.y - 1) << " " << (p1.x - 1) << " "
	 << (p2.y - 1) << " " << (p2.x - 1);
  return ss.str();
}

vector<string> SameColorPairs::removePairs(const vector<string> &board_str)
{
  Board board(board_str);
	
  vector<string> ret;

  int iter = 0;
  while (true)
  {
	const int prev_size = ret.size();
	
	vector<Pos> pos_list;
	select_order(board, pos_list, iter);
	
	for (const Pos &p : pos_list)
	{
	  if (board.visited(p) == NOT_USED)
	  {
		bool deleted = false;
		for (int i = 0; i < 4; i++)
		{
		  if (board.color(p) == board.next(p, i)->color && board.next(p, i)->visited == NOT_USED)
		  {
			board.remove(p);
			board.visited(p) = USED;
		  
			board.remove(board.next(p, i));
			board.next(p, i)->visited = USED;

			ret.push_back(to_string(p, board.next(p, i)->pos));

			if (IS_DEBUG) cerr << ret.back() << endl;
			
			deleted = true;
			break;
		  }
		}
		if (!deleted)
		{
		  vector<Cell *> s;
		  if (match(board, p, 0, s))
		  {
			assert(!s.empty());
			assert(s.size() % 2 == 0);

			for (int i = 0; i < s.size() / 2; i++)
			{
			  ret.push_back(to_string(s[2*i]->pos, s[2*i+1]->pos));
			  if (IS_DEBUG) cerr << ret.back() << endl;
			}		
		  }
		  if (IS_DEBUG) cerr << "====================" << endl;
		}
	  }
	}
	if (ret.size() == prev_size)
	{
	  break;
	}
	iter++;
  }
  return ret;
}

// -------8<------- end of solution submitted to the website -------8<-------

#ifdef DEBUG

template<class T> void getVector(vector<T>& v)
{
  for (int i = 0; i < v.size(); ++i)
  {
	cin >> v[i];
  }
}

int main()
{
  SameColorPairs scp;
  int H;
  cin >> H;
  vector<string> board(H);
  getVector(board);

  vector<string> ret = scp.removePairs(board);
  cout << ret.size() << endl;
  for (int i = 0; i < (int)ret.size(); ++i)
  {
	cout << ret[i] << endl;
  }
  cout.flush();
}

#endif
